<section id="banner" style="background: url(<?=Yii::$app->homeUrl?>images/background.jpg) center no-repeat;">
    <article class="opacity">
        <h1>Customise suas músicas</h1>
        <h1>Crie sua própria Mix</h1>
        <p>Você está em um portal para um novo universo de músicas<br>
        <span class="pink">FREE ROYALTY</span> e <span class="blue">TRILHAS EXCLUSIVAS</span></p>
    </article>

    <article class="search">
        <input type="text" placeholder="Busca">
    </article>
</section>

<div class="gray">
    <div class="center">
        <h1>O que você procura em três passos</h1>

        <section class="steps">
            <span class="box">Música</span>
            <i class="icon-separator"></i>
            <span class="box">Estilo</span>
            <i class="icon-separator"></i>
            <span class="box">Modalidade</span>
            <div class="clear"></div>
        </section>
    </div>
</div>

<div class="center">
    <figure class="title">
        <img src="<?=Yii::$app->homeUrl?>images/logo-black.png" alt="Prime Jingles">
        <figcaption>&nbsp;&bull; Special Features &bull;</figcaption>
        <div class="clear"></div>
    </figure>

    <section class="special-features">
        <div class="half left">
            <article class="gray">
                <h2>100% Customisável</h2>
            </article>

            <article class="gray">
                <p>Crie sua propria mix usando seu programa de edição favorito com o Open Mix Pack</p>
            </article>

            <article class="gray">
                <h2>Formatos Inéditos</h2>
            </article>

            <article class="gray">
                <p>Crie sua propria mix usando seu programa de edição favorito com o Open Mix Pack</p>
            </article>

            <article class="gray">
                <h2>As Melhores Músicas</h2>
            </article>

            <article class="gray">
                <p>Todas as nossas músicas são criadas por nós no nosso própiro Estúdio.</p>
            </article>
        </div>

        <div class="half right">
            <article class="gray">
                <h2>Novas Trilhas</h2>
            </article>
            
            <article class="gray">
                <?//php $this->widget("application.widgets.Player") ?>
            </article>
        </div>

        <div class="clear"></div>
    </section>
</div>

<section class="who-we-are">
    <h1 class="gray">Leve o nosso estúdio pra dentro da sua produtora!</h1>

    <article>
        <div class="center">
            <div class="col first-child">
                Com os pacotes Open Mix e Open Mix Plus, você terá a real sensação de ter um estúdio de gravação anexado a sua ilha de edição.
            </div>

            <div class="col">
                Se você precisa de algo ainda mais exclusivo, ou uma ajuda nas suas mixagens e masterização, entre em contato com a gente.
            </div>

            <div class="col">
                Vá além do que a nossa biblioteca pode oferecer. Edite cada instrumento separadamente, e faça o Sound Design perfeito para o seu filme.
            </div>

            <div class="clear"></div>

            <h2 class="title">Nossa Tecnologia</h2>

            <div class="col first-child">
                <figure>
                    <img src="<?=Yii::$app->homeUrl?>images/daw.png" alt="DAW">
                    
                    <figcaption>
                        <h2>DAW</h2>
                        <p>
                            Pro Tools - Logic Pro X<br>
                            Reaper - Finale
                        </p>
                    </figcaption>
                </figure>
            </div>

            <div class="col">
                <figure>
                    <img src="<?=Yii::$app->homeUrl?>images/monitor.png" alt="Monitores">
                    
                    <figcaption>
                        <h2>Monitores</h2>
                        <p>
                            Yamaha 5.1 - Jbl<br>
                            Sennheiser - Akg
                        </p>
                    </figcaption>
                </figure>
            </div>

            <div class="col">
                <figure>
                    <img src="<?=Yii::$app->homeUrl?>images/microphone.png" alt="Microfones">
                    
                    <figcaption>
                        <h2>Microfones</h2>
                        <p>
                            Blue - Audix - Shure<br>
                            Sennheiser - Neumman - Akg
                        </p>
                    </figcaption>
                </figure>
            </div>

            <div class="clear"></div>

            <h2 class="title">Customer Services</h2>

            <div class="col first-child">
                <figure>
                    <img src="<?=Yii::$app->homeUrl?>images/mix.png" alt="Mix/Master">
                    
                    <figcaption>
                        <h2>Mix/Master</h2>
                        <p>
                            Mixagem e Masterização Analógica para as suas Customizações de Open Mix e outros trabalhos
                        </p>
                    </figcaption>
                </figure>
            </div>

            <div class="col">
                <figure>
                    <img src="<?=Yii::$app->homeUrl?>images/record.png" alt="Gravação">
                    
                    <figcaption>
                        <h2>Gravação</h2>
                        <p>
                            Gravação de uma musica especial para seu job e Customização do nosso acervo de modo personalizado
                        </p>
                    </figcaption>
                </figure>
            </div>

            <div class="col">
                <figure>
                    <img src="<?=Yii::$app->homeUrl?>images/editing.png" alt="Edição">
                    
                    <figcaption>
                        <h2>Edição</h2>
                        <p>
                            Tratamento de ruídos de Som Direto, frequências e interferências de rádio e automações para quaiquer JOBs
                        </p>
                    </figcaption>
                </figure>
            </div>
        </div>

        <div class="clear"></div>
    </article>
</section>

<section id="portfolio">
    <div class="center">
        <h2>Portfolio de Trilhas Originais</h2>

        <article class="video">
            <iframe src="https://player.vimeo.com/video/112936080" width="330" height="" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </article>

        <article class="video">
            <iframe src="https://player.vimeo.com/video/112936080" width="330" height="" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </article>

        <article class="video">
            <iframe src="https://player.vimeo.com/video/112936080" width="330" height="" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </article>

        <div class="clear"></div>
    </div>
</section>

<section class="bg-image">
    <div class="center">
        <h2><img src="<?=Yii::$app->homeUrl?>images/logo-black.png" alt="Prime Jingles"><span>&bull; Prime Jingles &bull;</span></h2>

        <p>
            <span class="small">Muito mais do que uma apenas uma seleção feita por curadores...</span><br>
            <span class="normal">Uma biblioteca 100% arquitetada por produtores</span><br>
        </p>
        <p class="blue">
            O melhor acervo de músicas<br>
            <span class="font-pink">Free Royalty</span> e <span class="font-white">Trilhas Exclusivas</span>
        </p>
    </div>
</section>

<section class="newsletter">
    <div class="center">
        <p>Inscreva-se no nosso Newsletter e fique<br>
        por dentro das novidades e promoçoes!</p>

        <form action="">
            <div class="social">
                <img src="<?=Yii::$app->homeUrl?>images/facebook.png" alt="Facebook">
                <img src="<?=Yii::$app->homeUrl?>images/vimeo.png" alt="Vimeo">
                <img src="<?=Yii::$app->homeUrl?>images/twitter.png" alt="Twitter">
                <img src="<?=Yii::$app->homeUrl?>images/instagram.png" alt="Instagram">
            </div>

            <label for="email">Por favor, insira um email válido</label><br>

            <input type="email" name="email">
            <input type="submit" value="Assine Agora">

            <div class="clear"></div>
        </form>
    </div>
</section>
