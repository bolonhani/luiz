<section id="banner" style="background: url(<?=Yii::$app->homeUrl?>images/background-features.jpg) center no-repeat;">
    <article class="opacity features">
        <h1>&bull; THE PRIME JINGLES &bull;</h1>
        <p>Uma biblioteca 100% arquitetada por produtores<br>
    </article>
</section>

<div class="gray">
    <div class="center">
        <h1 class='features'>Compartilhando Criatividade</h1>
    </div>
</div>

<div class='border-line'>
    <div class="center">

        <section class="sharing-creativity">
            <p>Nossa biblioteca foi arquitetada para te dar a liberdade criativa, como se voce tivesse encomendando uma trilha original de um estúdio, 
            mas sem pagar o preço de uma trilha exclusiva e sem perder tempo com a logistica de criação e aprovação do cliente.</p>
            <p>Além de músicas incriveis, nosso estudio oferece a você uma novo ponto de vista para a escolha das músicas para o seu filme.</p>

        </section>
    </div>
</div>

<div class="center">
    <figure class="title-features">
        <img src="<?=Yii::$app->homeUrl?>images/logo-black.png" alt=" OPEN MIX e OPEN MIX PLUS" align='left'>
        <figcaption>&nbsp;&bull; OPEN MIX e OPEN MIX PLUS &bull;</figcaption>
        <div class="clear"></div>
    </figure>
    <section class='open-mix'>
        <figure class='inner-text'>
            <img src="<?=Yii::$app->homeUrl?>images/open-mix.png" alt=''>
            <div class="clear"></div>
        </figure>
        <div class='col'>
            <p>Com os pacotes Open Mix e Open Mix Plus, voce pode editar fades, criar novas passagens, destacar e silenciar um ou mais intrumentos, criar efeitos, 
            e usar a sua criatividade para criar uma trilha personalizada para o seu cliente.</p>

            <p>Tudo isso de forma prática e facil, apenas importando os arquivos de Wav para o seu programa de edição favorito - com o pacote Open Mix - ou com um duplo click no 
            arquivo Omf/AAF, Pro Tools, Reaper ou Logic Pro - com o pacote Open Mix Plus.</p>
        </div>
        <p>Na nossa biblioteca, você vai achar a melhor opção para a trilha do seu filme, como se vendêssemos os ingretientes e diversas receitas para o seu prato principal. 
        Você por ouvir as nossas músicas sob uma ótica sugestiva, e escolher, não só a melhor obra musical, mas a que contém os elementos musicais perfeitos para o seu filme.</p>
    </section>
</div>
<div class="clear"></div>
<div class='border-line-top gray'>
    <div class='center'>
        <div class='triangle gray border-black'></div>
        <section class='outlet-prime'>
            <h1>&bull; OUTLET PRIME JINGLES &bull;</h1>
            <p>O melhor preço com a melhor qualidade de mix!</p>
            <div class='price' style="background: url(<?=Yii::$app->homeUrl?>images/drums.png) center no-repeat;">
                <article >
                    <ul class="opacity">
                        <li>Música a partir de <h2>RS<strong>29.00</strong></h2></li>
                        <li>Open Mix a partir de <h2>RS<strong>89.00</strong></h2></li>
                    </ul>
                </article>
            </div>
        </section>
    </div>
</div>
<div class="clear"></div>
<div class='blue'>
    <div class='center'>
        <div class='triangle blue border-white'></div>
        <section class='solution'>
            <h1>A Solução Completa Para o Seu Áudio</h1>
            <p>Se estiver perdido em meio às ferramentas do Prime Jingles ou se a sua rotina de trabalho anda muito agitada, não tem problema, nós arrumamos tudo para você!</p>

            <p>O Prime Jingles detém um estúdio moderno e equipado com tecnologia de última geração – monitores high-end, tratamento acústico, pré-amplificadores valvulados, 
            conversores digitais dedicados ao áudio e muito mais. Assim, conseguimos aliar rapidez a qualidade, tanto no formato digital quanto no analógico.</p>

            <p>Caso prefira que nós façamos a customização da sua música preferida, basta entrar em contato conosco. Podemos inserir ou trocar instrumentos, mudar efeitos, 
            volumes, tempo e muito mais – sempre com qualidade e agilidade!</p>
            <div class='border-bottom-white'>
                <h1>Customer Services</h1>
            </div>
            <ul>
                <li>
                    <figure>
                        <img src="<?=Yii::$app->homeUrl?>images/mix.png">
                    </figure>
                    <div>MIX/MASTER</div>
                    <p>Caso você precise de uma ajuda para customizar os pacotes Open Mix e Open Mix Plus; a Masterização do seu Track, ou a mixagem do seu filme em Stereo ou Surround 5.1.</p>
                </li>
                <li>
                    <figure>
                        <img src="<?=Yii::$app->homeUrl?>images/record.png">
                    </figure>
                    <div>GRAVAÇÃO</div>
                    <p>Podemos criar uma música exclusiva para o seu filme, um Logo Audio ou um Jingle para a sua campanha; um tema de abertura para series, documentarios, filmes, news e muito mais. </p>
                </li>
                <li>
                    <figure>
                        <img src="<?=Yii::$app->homeUrl?>images/editing.png">
                    </figure>
                    <div>EDIÇÃO</div>
                    <p>Oferecemos também os serviços de tratamento e redução de interferências e ruídos ambientes, compressão e equalização de audios ON e OFF para o seu Som Direto e narração.</p>
                </li>
            </ul>
            <div class="clear"></div>
        </section>
    </div>
</div>
<div class='white'>
    <div class='border-line-top-small'>
        <div class='center'>
            <section class='licence'>
                <h1>Licenciamento Simples</h1>
                <p>Pensando em sua correria diária e no valor do seu suor, a equipe do Prime Jingles simplificou a burocracia do licenciamento. 
                Aqui você escolhe entre três licenças que abrangem todos os meios de comunicação convencionais.</p>
                <div class="clear"></div>
            </section>
        </div>
    </div>
</div>
<div class='border-line-top-small gray'>
    <div class='center'>
        <div class='triangle gray border-black'></div>
        <section class='licence-info'>
            <h1>Up Grade de Licenças</h1>
            <p>Você pode apenas comprar uma nova licença e assim organizar melhor seus trabalhos – isso sem falar na economia, afinal você não precisa trocar de carro a cada troca de pneus, não é mesmo?</p>
            <h1>Licenças Perpétuas</h1>
            <p>Compre uma vez no Prime Jingles, publique de acordo com a licença correta, e divulgue seu Case sem limite de tempo.</p>
            <h1>Seguro para Vimeo, Youtube, Facebook e outras Redes Sociais</h1>
            <p>Todas as nossas licenças oferecem suporte legal para a publicação em quaisquer redes sociais.</p>
            <h1>Compare e escolha a melhor licença para o seu Case:</h1>
            <table>
                <tr>
                    <th><strong>Mídia de Publicação</strong></th>
                    <th><strong>Licença Standard</strong><br>Ver Licença Completa</th>
                    <th><strong>Licença Premium</strong><br>Ver Licença Completa</th>
                    <th><strong>Licença Master</strong><br>Ver Licença Completa</th>
                </tr>
                <tr>
                    <td>Vimeo, Youtube, Facebook, Apps, Web e Games</td>
                    <td class='blue center'>&radic;</td>
                    <td class='blue center'>&radic;</td>
                    <td class='blue center'>&radic;</td>
                </tr>
                <tr>
                    <td>Tv, Radio Broadcastig</td>
                    <td class='pink center'>X</td>
                    <td class='center'>Conteúdo Background para: Documentários, Reality Shows, Noticiários, Seriados e Novelas.</td>
                    <td class='center'>Tema de abertura de Séries, Novelas, Seriados, Noticiarios, e etc.</td>
                </tr>
                <tr>
                    <td>Cinema e Teatro</td>
                    <td class='center'>Independente</td>
                    <td class='center'>Independente</td>
                    <td class='center'>Venda de Ingressos e Filmes e Peças comerciais, e/ou patrocinados</td>
                </tr>
                <tr>
                    <td>Downloads e Copias Físicas</td>
                    <td class='center'>Limite de 10.000</td>
                    <td class='center'>Limite de 10.000</td>
                    <td class='center'>Ilimitado</td>
                </tr>
                <tr>
                    <td>Transação Comercial e Distribuição Fisica (Dvd - Bluray - PenDrive e Etc)</td>
                    <td class='center'>Downloads e Distribuição Gratuitos</td>
                    <td class='center'>Downloads e Distribuição Gratuitos</td>
                    <td class='blue center'>&radic;</td>
                </tr>
                <tr>
                    <td>Publicidade, Merch, Logo Marca</td>
                    <td class='pink center'>X</td>
                    <td class='pink center'>X</td>
                    <td class='blue center'>&radic;</td>
                </tr>
            </table>
        </section>
    </div>
</div>
<div class='background-full-prime-jingles-features' style="background: url(<?=Yii::$app->homeUrl?>images/background2.jpg) center no-repeat;">
    <div class='center'>
        <section class='prime-jingles-features'>
            <figure class="title-features">
                <img src="<?=Yii::$app->homeUrl?>images/logo.png" alt=" OPEN MIX e OPEN MIX PLUS" align='left'>
                <figcaption>&nbsp;&bull; PRIME JINGLES &bull;</figcaption>
            </figure>
            <div class="clear"></div>
            <article class='opacity'>
                <h2>Nosso foco é a excelencia em suprir as suas reais necessidades</h2>
                <h1>Uma biblioteca 100% arquitetada por produtores</h1>

            </article>
            <div class='caption'>
                <p>O melhor acervo de músicas</p>
                <strong class='font-pink'>FREE ROYALTY</strong> e <strong class='font-white'>TRILHAS EXCLUSIVAS </strong>
            </div>
        </section>
    </div>
</div>
<div class='clear'></div>
<div class='center'>
    <section class='connect'>
        <h1>Criatividade Compartilhada - Conecte-se com a gente</h1>
    </section>
</div>
<div class='gray'>
    <div class='center'>
        <section class='end-features'>
            <p>Estamos totalmente comprometidos em suprir todas as necessidades musicais de seu trabalho. Por esse motivo, nossas composições estão de acordo com as demandas nacionais e internacionais. Estamos sempre ouvindo as produtoras e nos adaptando às suas necessidades.</p>
            <p>Gostaríamos de saber a sua opinião, para apresentar um trabalho de acordo com as reais demandas do mercado e oferecer sempre a melhor opção de compras de músicas corporativas.</p>
            <p>Muito obrigado por escolher o Prime Jingles. Estamos sempre trabalhando para facilitar seu workflow!</p>
            <p>Att.,</p>
            <p>Rafael Valleri CEO/Audio Engeneer/Song Writer and Programing.<br>
            Ariel Padula CEO/Dir. Composition/Song Writer/Recording Performer.</p>
            <iframe src="http://www.google.com.br/maps?f=q&source=s_q&hl=pt-BR&geocode=&q=R.+Paracuê,+159&aq=&sll=-23.54671,-46.6873074&sspn=63.915145,117.246094&ie=UTF8&hq=&hnear=R.+Paracuê,+159+-+Sumaré,+São+Paulo+-+SP,+01257-050&t=m&z=14&ll=-23.54671,-46.6873074&output=embed" width="800" height="200" frameborder="0" style="border:0"></iframe>
            <div class='icons'>
                <a href='https://facebook.com' target="_blank">
                    <figure>
                        <img src="<?=Yii::$app->homeUrl?>images/facebook-grande.png">
                    </figure>
                </a>
                <a href='https://vimeo.com' target="_blank">
                    <figure>
                        <img src="<?=Yii::$app->homeUrl?>images/vimeo-grande.png">
                    </figure>
                </a>
                <a href='https://twitter.com' target="_blank">
                    <figure>
                        <img src="<?=Yii::$app->homeUrl?>images/twitter-grande.png">
                    </figure>
                </a>
                <a href='instagram' target="_blank">
                    <figure>
                        <img src="<?=Yii::$app->homeUrl?>images/instagram-grande.png">
                    </figure>
                </a>
            </div>
        </section>
    </div>
</div>

