<div class='gray'>
	<div class='center'>
		<section class='contact'>
			<div class='contact-info'>
				<h1>Get in touch</h1>
				<h2>Company Name</h2>
				<address>
					2601 Mission St.<br>
					San Francisco, CA 94110 <br>
					info@mysite.com <br>
					Tel: 123-456-7890 <br>
					Fax: 123-456-7890 <br>
				</address>
				<div class='icones'>
					<a href='https://www.facebook.com' target="_blank">
						<figure>
							<img src="<?=Yii::$app->homeUrl?>images/facebook-pequeno.png">
						</figure>
					</a>
					<a href='https://www.twitter.com' target="_blank">
						<figure>
							<img src="<?=Yii::$app->homeUrl?>images/twitter-pequeno.png">
						</figure>
					</a>
					<a href='https://plus.google.com' target="_blank">
						<figure>
							<img src="<?=Yii::$app->homeUrl?>images/google-pequeno.png">
						</figure>
					</a>
				</div>
			</div>
			<iframe src="http://www.google.com.br/maps?f=q&source=s_q&hl=pt-BR&geocode=&q=R.+Paracuê,+159&aq=&sll=-23.54671,-46.6873074&sspn=63.915145,117.246094&ie=UTF8&hq=&hnear=R.+Paracuê,+159+-+Sumaré,+São+Paulo+-+SP,+01257-050&t=m&z=14&ll=-23.54671,-46.6873074&output=embed" width="600" height="600" frameborder="0" style="border:0"></iframe>
		</section>
		<div class='clear'></div>
	</div>
</div>