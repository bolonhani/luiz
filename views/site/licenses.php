<section id="banner" style="background: url(<?=Yii::$app->homeUrl?>images/background-outlet.jpg) 25% 0% no-repeat; background-size:100%;">
    <article class="opacity license">
        <h1>Licenciamento Simples</h1>
        <p>Pensando em sua correria diária e no valor do seu suor, a equipe do Prime Jingles simplificou a burocracia do licenciamento. 
        Aqui você escolhe entre três licenças que abrangem todos os meios de comunicação convencionais.</p>
    </article>
</section>
<div class='clear'></div>
<div class='border-line border-line-top-small'>
	<div class='center'>
		<section class='license'>
			<h1>Conheça as nossas Licenças</h1>
			<ul class='blue-big'>
	            <li>
	                <div>Licença Standard</div>
	            </li>
	            <li>
	                <div>Licença Premium</div>
	            </li>
	            <li>
	                <div>Licença Master</div>
	            </li>
	        </ul>
	        <h2>Suporte de Publicação</h2>
	        <ul class='black-small'>
	            <li>
	                <div>Música</div>
	            </li>
	            <li>
	                <div>Efeitos Sonoros</div>
	            </li>
	            <li>
	                <div>Audio Logo</div>
	            </li>
	        </ul>
		</section>
		<div class="clear"></div>
	</div>
</div>
<div class="border-line-top-small gray">
	<div class='center'>
		<div class='triangle gray border-black'></div>
			<figure class="title-license">
        		<img src="<?=Yii::$app->homeUrl?>images/logo-black.png" alt=" OPEN MIX e OPEN MIX PLUS" align='left'>
        		<figcaption>&nbsp;&bull; FREE ROYALTY MUSIC &bull;</figcaption>
        	<div class="clear"></div>
    	</figure>
    	<section class='licence-info'>
    		<table>
                <tr>
                    <th><strong>Mídia de Publicação</strong></th>
                    <th><strong>Licença Standard</strong><br>Ver Licença Completa</th>
                    <th><strong>Licença Premium</strong><br>Ver Licença Completa</th>
                    <th><strong>Licença Master</strong><br>Ver Licença Completa</th>
                </tr>
                <tr>
                    <td>Vimeo, Youtube, Facebook, Apps, Web e Games</td>
                    <td class='blue center'>&radic;</td>
                    <td class='blue center'>&radic;</td>
                    <td class='blue center'>&radic;</td>
                </tr>
                <tr>
                    <td>Open Mix - Open Mix Plus <br> Support e Disponibilidade</td>
                    <td class='blue center'>&radic;</td>
                    <td class='blue center'>&radic;</td>
                    <td class='blue center'>&radic;</td>
                </tr>
                <tr>
                    <td>Tv, Radio Broadcastig</td>
                    <td class='pink center'>X</td>
                    <td class='center'>Conteúdo Background para: Documentários, Reality Shows, Noticiários, Seriados e Novelas.</td>
                    <td class='center'>Tema de abertura de Séries, Novelas, Seriados, Noticiarios, e etc.</td>
                </tr>
                <tr>
                    <td>Cinema e Teatro</td>
                    <td class='center'>Independente</td>
                    <td class='center'>Independente</td>
                    <td class='center'>Venda de Ingressos e Filmes e Peças comerciais, e/ou patrocinados</td>
                </tr>
                <tr>
                    <td>Downloads e Copias Físicas</td>
                    <td class='center'>Limite de 10.000</td>
                    <td class='center'>Limite de 10.000</td>
                    <td class='center'>Ilimitado</td>
                </tr>
                <tr>
                    <td>Transação Comercial e Distribuição Fisica (Dvd - Bluray - PenDrive e Etc)</td>
                    <td class='center'>Downloads e Distribuição Gratuitos</td>
                    <td class='center'>Downloads e Distribuição Gratuitos</td>
                    <td class='blue center'>&radic;</td>
                </tr>
                <tr>
                    <td>Publicidade, Merch, Logo Marca</td>
                    <td class='pink center'>X</td>
                    <td class='pink center'>X</td>
                    <td class='blue center'>&radic;</td>
                </tr>
            </table>
        </section>
	</div>
</div>
<div class='blue'>
	<div class="center">
		<div class="triangle blue border-gray"></div>
		<section class='sound-effects'>
			<h1><img src='<?=Yii::$app->homeUrl?>images/thunder.png'> EFEITOS SONOROS <img src='<?=Yii::$app->homeUrl?>images/thunder.png'></h1>
			<p>Simples assim: Você só não pode revender nossos Efeitos Sonoros nem usa-los como Audio Logo. </p>
			<p>No Check Out, você irá perceber que nosso sistema habilita apenas a Licença Standard, mas a cobertura da Licença Standard para os Efeitos Sonoros é maior do que para músicas.</p>
			<p>Clique aqui para ler a Licença de Efeitos Sonoros completa</p>
			
		</section>
	</div>
</div>
<div class='gray-3'>
	<div class='center'>
		<div class="triangle border-blue"></div>
		<section class='logo-audio'>
			<h1><img src='<?=Yii::$app->homeUrl?>images/logo.png'>&bull; AUDIO LOGOS &bull;<img src='<?=Yii::$app->homeUrl?>images/logo.png'></h1>
			<p>Todos os Audio Logos estão sob o suporte da Licença Master. No Check Out, você irá perceber que nosso sistema habilita apenas a Licença Master.</p>
			<p>Caso você queira um Audio Logo Exclusivo, entre em contato para checar a disponibilidade, ou para encomendar um Audo Logo Original</p>
			<p>Clique aqui para ler a Licença Master completa</p>
		</section>
	</div>
</div>
<div class='white'>
    <section class='end-outlet'>
    </section>        
</div>