
<div class='background-full-faq' style="background: url(<?=Yii::$app->homeUrl?>images/background-faq.png) center no-repeat; background-size:100%;">
	<section id="banner">
	    <article class="opacity faq">
	        <h1>Costumer Support</h1>
	        <p>Encontre aqui as principais perguntas e respostas. Se você ainda precisar de uma atenção especial com qualquer assunto, entre em contato, que responderemos o mais rápido possivel</p>
	    </article>
	</section>
</div>
<div class='clear'></div>
<div class='center'>
	<section class='frequent-ask-questions'>
		<h1>Frequently Ask Questions</h1>
		<ul>
            <li>
                <div>Nomenclatura</div>
            </li>
            <li>
                <div>Download</div>
            </li>
            <li>
                <div>Tech Support</div>
            </li>
            <li>
                <div>Autorização</div>
            </li>
            <li>
                <div>Pagamentos</div>
            </li>
            <li>
                <div>Publicação</div>
            </li>
        </ul>
	</section>
</div>
<div class='clear'></div>
<div class='border-line border-line-top-small'>
	<div class='center'>
		<section class='licence-faq'>
			<h1>Licenciamento Simples</h1>
			<p>Pensando em sua correria diária e no valor do seu suor, a equipe do Prime Jingles simplificou a burocracia do licenciamento. 
			Aqui você escolhe entre três licenças que abrangem todos os meios de comunicação convencionais.</p>
		</section>
	</div>
</div>
<div class='border-line-top-small gray'>
    <div class='center'>
        <div class='triangle gray border-black'></div>
        <section class='licence-info'>
            <table>
                <tr>
                    <th><strong>Mídia de Publicação</strong></th>
                    <th><strong>Licença Standard</strong><br>Ver Licença Completa</th>
                    <th><strong>Licença Premium</strong><br>Ver Licença Completa</th>
                    <th><strong>Licença Master</strong><br>Ver Licença Completa</th>
                </tr>
                <tr>
                    <td>Vimeo, Youtube, Facebook, Apps, Web e Games</td>
                    <td class='blue center'>&radic;</td>
                    <td class='blue center'>&radic;</td>
                    <td class='blue center'>&radic;</td>
                </tr>
                <tr>
                    <td>Open Mix - Open Mix Plus <br> Support e Disponibilidade</td>
                    <td class='blue center'>&radic;</td>
                    <td class='blue center'>&radic;</td>
                    <td class='blue center'>&radic;</td>
                </tr>
                <tr>
                    <td>Tv, Radio Broadcastig</td>
                    <td class='pink center'>X</td>
                    <td class='center'>Conteúdo Background para: Documentários, Reality Shows, Noticiários, Seriados e Novelas.</td>
                    <td class='center'>Tema de abertura de Séries, Novelas, Seriados, Noticiarios, e etc.</td>
                </tr>
                <tr>
                    <td>Cinema e Teatro</td>
                    <td class='center'>Independente</td>
                    <td class='center'>Independente</td>
                    <td class='center'>Venda de Ingressos e Filmes e Peças comerciais, e/ou patrocinados</td>
                </tr>
                <tr>
                    <td>Downloads e Copias Físicas</td>
                    <td class='center'>Limite de 10.000</td>
                    <td class='center'>Limite de 10.000</td>
                    <td class='center'>Ilimitado</td>
                </tr>
                <tr>
                    <td>Transação Comercial e Distribuição Fisica (Dvd - Bluray - PenDrive e Etc)</td>
                    <td class='center'>Downloads e Distribuição Gratuitos</td>
                    <td class='center'>Downloads e Distribuição Gratuitos</td>
                    <td class='blue center'>&radic;</td>
                </tr>
                <tr>
                    <td>Publicidade, Merch, Logo Marca</td>
                    <td class='pink center'>X</td>
                    <td class='pink center'>X</td>
                    <td class='blue center'>&radic;</td>
                </tr>
            </table>
        </section>
    </div>
</div>