<div class='gray'>
	<div class='center'>
		<section class='introduction'>
			<figure>
				<img src="<?=Yii::$app->homeUrl?>images/logo-black.png">
			</figure>
			<h1>Oi Leonardo Nagasaki, Tudo bem?</h1>
			<p>Essa é sua pagina de usuário Prime Jingles. Aqui você pode ver a sua atividade recente, editar suas listas, ver os seus downloads disponíveis, número do seu pedido - que é importante 
			para efetuar um upgrade de licenças, cupons de desconto e muito mais. Todas as tarefas disponiveis estão separadas por grupos. Você só precisa clicar 
			no grupo de informações que deseja e tudo está a sua disposição, de forma clara e simples. </p>
		</section>
		<div class='clear'></div>
		<section class='quick-links'>
			<h1>Quick Links <strong>&gt;</strong></h1>
			<a href=''><div>Minhas Compras</div></a> <a href=''><div>Meus Downloads</div></a>
		</section>
		<div class='clear'></div>
	</div>
</div>
<div class='white'>
    <div class='border-line-top-small'>
        <div class='center'>
        	<section class='first-step-favorite'>
        		<h2>Primeiro, escolha o que você quer ouvir: </h2>
        		<ul>
        			<li>
		                <div>Música</div>
		            </li>
		            <li>
		                <div>Audio Logo</div>
		            </li>
		            <li>
		                <div>Efeitos Sonoros</div>
		            </li>
        		</ul>
        	</section>
		</div>
	</div>
</div>
<div class='clear'></div>
<div class='border-line-top-small gray'>
    <div class='center'>
        <div class='triangle gray border-black'></div>
        <section class='first-step-favorite'>
        	<h2>Agora, navegue pelo conteúdo que você escolheu:</h2>
        	<ul class='small'>
    			<li>
	                <div>Minha Playlist</div>
	            </li>
	            <li>
	                <div>Minhas Favoritas</div>
	            </li>
	            <li>
	                <div>Atividades Recentes</div>
	            </li>
    		</ul>
    		<div class='clear'></div>
    		<div class='img' style="background: url(<?=Yii::$app->homeUrl?>images/img-login.jpg) no-repeat center top; background-size:800px;">
    		<div class='clear'></div>
    			<article>
					<h1 class='opacity'>Do jeito que Brasileiro gosta!</h1>
					<ul>
						<li class='opacity'><strong class='blue'>5% OFF</strong> Acima de 5 Músicas</li>
						<li class='opacity'><strong class='pink'>10% OFF</strong> Acima de 10 Músicas</li>
					</ul>
				</div>
				</article>
			<div class='clear'></div>
        </section>
    </div>
</div>
