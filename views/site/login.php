<div class='gray'>
	<div class='center'>
		<section class='login'>
			<fieldset>
				<h1>Log-in</h1>
				<form>
					<input type='email' placeholder='Email'>
					<input type='password' placeholder='senha'>
				</form>
			</fieldset>
			<div class='img-login' style="background: url(<?=Yii::$app->homeUrl?>images/img-login.jpg) center no-repeat; background-size:650px;">
				<h1 class='opacity'>Do jeito que Brasileiro gosta!</h1>
				<ul>
					<li class='opacity'><strong class='blue'>5% OFF</strong> Acima de 5 Músicas</li>
					<li class='opacity'><strong class='pink'>10% OFF</strong> Acima de 10 Músicas</li>
				</ul>
			</div>
			<fieldset>
				<h1>Novo Usuário</h1>
				<form>
					<input type='text' placeholder='Nome'>
					<input type='email' placeholder='Email'>
					<input type='password' placeholder='senha'>
					<input type='checkbox' id='accept' name='accept'><label for='accept'>Quero conhecer as musicas novas, participar de promoções e ganhar cupons de desconto</label>
				</form>
			</fieldset>
			<div class='clear'></div>
		</section>
	</div>
</div>
<div class='clear'></div>