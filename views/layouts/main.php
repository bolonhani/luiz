<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <header>
        <section class="top">
            <nav>
                <ul>
                    <li><a href="<?=URL::to(['favoritos/'])?>">Fav / Playlist</a><span class="separator">|</span></li>
                    <li><a href="<?=URL::to(['login/'])?>">Login / New User</a><span class="separator">|</span></li>
                    <li><a href="<?=URL::to(['carrinho/'])?>">My Cart: 9</a></li>
                </ul>
            </nav>
        </section>

        <div class="clear"></div>

        <section class="menu">
            <div class="center">
                <figure id="logo">
                    <img src="<?=URL::base()?>/images/logo.png" alt="Prime Jingles">

                    <figcaption>
                        &bull;Prime Jingles&bull;
                    </figcaption>

                    <div class="clear"></div>
                </figure>

                <nav>
                    <ul>
                        <li><a href="<?=URL::to(['musicas/'])?>">Músicas</a></li>
                        <li><a href="<?=URL::to(['audio-logos/'])?>">Audio Logos</a></li>
                        <li><a href="<?=URL::to(['efeitos-sonoros/'])?>">Efeitos Sonoros</a></li>
                        <li><a href="<?=URL::to(['features/'])?>">Features</a></li>
                        <li><a href="<?=URL::to(['outlet/'])?>">Outlet</a></li>
                        <li><a href="<?=URL::to(['licencas/'])?>">Licenças</a></li>
                        <li><a href="<?=URL::to(['faq/'])?>">FAQ</a></li>
                        <li><a href="<?=URL::to(['contato/'])?>">Contato</a></li>
                    </ul>
                </nav>

                <div class="clear"></div>
            </div>
        </section>
    </header>

    <section id="content">

        <?= $content ?>
    </section>

    <footer>
        <div class="center">
            <ul>
                <li>Prime Jingles</li>
                <li><a href="">Músicas</a></li>
                <li><a href="">Efeitos Sonoros</a></li>
                <li><a href="">Áudio Logos</a></li>
                <li><a href="">Gêneros</a></li>
                <li><a href="">Modalidades</a></li>
                <li><a href="">Trilhas Exclusivas</a></li>
            </ul>

            <ul>
                <li>Free Royalty Music</li>
                <li><a href="">Epic/Cinematic</a></li>
                <li><a href="">Corporativo</a></li>
                <li><a href="">Piano Solo</a></li>
                <li><a href="">Folk</a></li>
                <li><a href="">Musica Clássica</a></li>
                <li><a href="">Eletronico</a></li>
            </ul>

            <ul>
                <li>Info</li>
                <li><a href="">Open Mix Packages</a></li>
                <li><a href="">Features</a></li>
                <li><a href="">Termos de Uso</a></li>
                <li><a href="">Licenciamento</a></li>
                <li><a href="">Mapa Do Site</a></li>
            </ul>

            <ul>
                <li>Support</li>
                <li><a href="">Contato</a></li>
                <li><a href="">FAQ</a></li>
                <li><a href="">Cue Sheets</a></li>
                <li><a href="">Mix/Master</a></li>
                <li><a href="">Open Mix Packages</a></li>
            </ul>

            <div class="clear"></div>
        </div>
    </footer>
    
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
