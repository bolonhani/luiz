<section id="banner" style="background: url(<?=Yii::$app->homeUrl?>images/background-outlet.jpg) 25% 8% no-repeat; background-size:100%;">
    <article class="opacity outlet">
        <h1>&bull; OUTLET &bull;</h1>
        <ul>
            <li>Músicas a partir de <strong>R$<b class='blue'>29.00</b></strong></li>
            <li>Open Mix a partir de <strong>R$<b class='blue'>89.00</b></strong></li>
        </ul>
        <div class='clear'></div>
        <p>O melhor preço com a melhor qualidade de mix!<br>
    </article>
</section>

<div class="center">
    <section id="music">
        <div class="player-large">
            <div class="controls">
                <i class="icon-fast-rewind"></i>
                <i class="icon-play-arrow"></i>
                <i class="icon-fast-forward"></i>

                <a class="info" href="javascript:void(0)">Play Recent Music</a>
            </div>

            <?//widget playerphp $this-widget('application.widgets.Player') ?>

            <div class="search-information">
                <p>Busca Ex: Música Feliz</p>
            </div>
            
            <div class="clear"></div>
        </div>

        <div class="listening">
            <div class="text-center">
                <ul>
                    <li>Ouvindo:</li>
                    <li><a href="" class="active">Músicas</a></li>
                    <li><a href="">Gêneros</a></li>
                    <li><a href="">Modalidades</a></li>
                    <li><a href="">Favoritos</a></li>
                    <li><a href="">Playlists</a></li>
                </ul>
                <div class="clear"></div>
            </div>
        </div>

        <div class="music-information">
            <div class="col-md-3">
                <div class="box-name height-1 col-md-12">
                    <p class="title">A Day to Remember</p>
                    <p class="description">Música suave e pra cima, evolutiva e com um final incrível!</p>
                </div>

                <div class="box-name height-1 col-md-12">
                    <p class="description">
                        Sample Rate: 44.1kHz / 24Bit<br>
                        Mp3: 176.4kHz 320kbps (4x 44.1kHz)<br>
                        Wav: 44.1kHz / 24Bit
                    </p>
                </div>
            </div>
            <div class="col-md-1">
                <div class="box-name height-1 col-md-12">
                    <p class="subtitle-2">Full Song</p>
                    <p class="description">
                        <button>&nbsp;</button>
                    </p>
                </div>

                <div class="box-name height-1 col-md-12">
                    <p class="subtitle-2">Shorts</p>
                    <p class="description">
                        <button>&nbsp;</button>
                        <button>&nbsp;</button>
                        <button>&nbsp;</button>
                    </p>
                </div>
            </div>

            <div class="col-md-2">
                <div class="box-name height-2 col-md-12">
                    <p class="subtitle">Loops</p>
                    <p class="description">
                        <button>&nbsp;</button>
                        <button>&nbsp;</button>
                        <button>&nbsp;</button>
                        <button>&nbsp;</button>
                        <button>&nbsp;</button>
                        <button>&nbsp;</button>
                        <button>&nbsp;</button>
                    </p>
                </div>
            </div>

            <div class="col-md-2">
                <div class="box-name height-2 col-md-12">
                    <p class="subtitle">Open Mix</p>
                    <p class="description">01 Drums, 02 Drive Guitar, 03 Clean Guitar, 01 Solo Guitar, 01 Bass, 01 Cello, 01 Synth, 01 Bell, 01 Piano</p>
                </div>
            </div>

            <div class="col-md-2">
                <div class="box-name height-2 col-md-12">
                    <p class="subtitle">Open Mix Plus</p>
                    <p class="description">
                        Opens Sessions:<br>
                        Pro Tools - Reaper - Logic Pro<br>
                        Other Versions:<br>
                        OMF - AAF - AIF - MP3 HD<br>
                        Avid Compatibility Enforced
                    </p>
                </div>
            </div>

            <div class="col-md-2">
                <div class="box-name height-2 col-md-12">
                    <br><br>
                    <a href="" class="subtitle">Add To List</a><br><br>
                    <a href="" class="subtitle">Add To Fav</a><br><br>
                    <a href="" class="subtitle">Share</a>
                </div>
            </div>
        </div>

        <div class="col-md-12 no-padding gray">
            <nav class="categories col-md-2 no-padding">
                <ul>
                    <li><a href="">Novidades</a></li>
                    <li><a href="">Popularidade</a></li>
                    <li><a href="">Cinematic/Epic</a></li>
                    <li><a href="">Corporativo</a></li>
                    <li><a href="">Musica Classica</a></li>
                    <li><a href="">Eletronico</a></li>
                    <li><a href="">Folk</a></li>
                    <li><a href="">Piano Solo</a></li>
                    <li><a href="">Rock Indie</a></li>
                    <li><a href="">Ritmos Brasileiros</a></li>
                    <li><a href="">Sound Design</a></li>
                </ul>
            </nav>

            <table class="default col-md-10">
                <tr>
                    <td><span class="blue">Álbum:</span> <span class="pink">Indie Pop</span></td>
                    <td class="center">Bpm</td>
                    <td class="center">Tempo</td>
                    <td class="center">Shorts</td>
                    <td class="center">Loops</td>
                    <td class="center">Open Mix</td>
                    <td class="center">Open Mix Plus</td>
                    <td class="center">Comprar</td>
                </tr>

                <tr class="music-row">
                    <td><span class="icon icon-play"></span><span class="middle">A Day To Remember</span></td>
                    <td class="center">130</td>
                    <td class="center">2:42</td>
                    <td class="center">3</td>
                    <td class="center">15</td>
                    <td class="center">12 Tracks</td>
                    <td class="center">7 Files</td>
                    <td class="center">R$ 100,00</td>
                </tr>

                <tr class="music-row">
                    <td><span class="icon icon-play"></span><span class="middle">A Day To Remember</span></td>
                    <td class="center">130</td>
                    <td class="center">2:42</td>
                    <td class="center">3</td>
                    <td class="center">15</td>
                    <td class="center">12 Tracks</td>
                    <td class="center">7 Files</td>
                    <td class="center">R$ 100,00</td>
                </tr>

                <tr class="music-row">
                    <td><span class="icon icon-play"></span><span class="middle">A Day To Remember</span></td>
                    <td class="center">130</td>
                    <td class="center">2:42</td>
                    <td class="center">3</td>
                    <td class="center">15</td>
                    <td class="center">12 Tracks</td>
                    <td class="center">7 Files</td>
                    <td class="center">R$ 100,00</td>
                </tr>

                <tr class="music-row">
                    <td><span class="icon icon-play"></span><span class="middle">A Day To Remember</span></td>
                    <td class="center">130</td>
                    <td class="center">2:42</td>
                    <td class="center">3</td>
                    <td class="center">15</td>
                    <td class="center">12 Tracks</td>
                    <td class="center">7 Files</td>
                    <td class="center">R$ 100,00</td>
                </tr>

                <tr class="music-row">
                    <td><span class="icon icon-play"></span><span class="middle">A Day To Remember</span></td>
                    <td class="center">130</td>
                    <td class="center">2:42</td>
                    <td class="center">3</td>
                    <td class="center">15</td>
                    <td class="center">12 Tracks</td>
                    <td class="center">7 Files</td>
                    <td class="center">R$ 100,00</td>
                </tr>

                <tr class="music-row">
                    <td><span class="icon icon-play"></span><span class="middle">A Day To Remember</span></td>
                    <td class="center">130</td>
                    <td class="center">2:42</td>
                    <td class="center">3</td>
                    <td class="center">15</td>
                    <td class="center">12 Tracks</td>
                    <td class="center">7 Files</td>
                    <td class="center">R$ 100,00</td>
                </tr>

                <tr class="music-row">
                    <td><span class="icon icon-play"></span><span class="middle">A Day To Remember</span></td>
                    <td class="center">130</td>
                    <td class="center">2:42</td>
                    <td class="center">3</td>
                    <td class="center">15</td>
                    <td class="center">12 Tracks</td>
                    <td class="center">7 Files</td>
                    <td class="center">R$ 100,00</td>
                </tr>
            </table>
        </div>

        <div class="clear"></div>
    </section>
</div>
<div class='gray'>
    <section class='end-outlet'>
    </section>        
</div>