<?php
use yii\db\Schema;
use yii\db\Migration;

class m150426_180256_init extends Migration {
	public function up() {
		$this->createTable ( 'administrator', array (
				'id' => 'pk',
				'username' => 'string NOT NULL',
				'password_hash' => 'string NOT NULL',
				'created_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
				'updated_at' => 'timestamp' 
		) );
		
		$this->insert ( 'administrator', array (
				'username' => 'admin',
				'password_hash' => '$2a$13$zvmIUclS6joNg2cUBuA1Xu1O09Mu6NHRLmPWjo9c22Vov.TMKg4M6' 
		) );
		
		$this->createTable ( 'user', array (
				'id' => 'pk',
				'name' => 'string NOT NULL',
				'email' => 'string NOT NULL',
				'cpf' => 'int(255)',
				'password_hash' => 'string NOT NULL',
				'active' => 'int(1) NOT NULL DEFAULT 0',
				'created_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
				'updated_at' => 'timestamp' 
		) );
		
		$this->createTable ( 'category', array (
				'id' => 'pk',
				'name' => 'string NOT NULL',
				'section' => 'enum("musicas","audio_logos","efeitos_sonoros","") NOT NULL',
				'created_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
				'updated_at' => 'timestamp' 
		) );
		
		$this->createTable ( 'track', array (
				'id' => 'pk',
				'name' => 'string NOT NULL',
				'description' => 'string NOT NULL',
				'tech_description' => 'string NOT NULL',
				'time' => 'int NOT NULL',
				'bpm' => 'int NOT NULL',
				'tech_description' => 'string NOT NULL',
				'standard_price' => 'float NOT NULL',
				'premium_price' => 'float NOT NULL',
				'master_price' => 'float NOT NULL',
				'created_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
				'updated_at' => 'timestamp' 
		) );
		
		$this->createTable ( 'album', array (
				'id' => 'pk',
				'name' => 'string NOT NULL',
				'created_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
				'updated_at' => 'timestamp' 
		) );
		
		$this->createTable ( 'album_category', array (
				'id_album' => 'int NOT NULL',
				'id_category' => 'int NOT NULL' 
		) );
		
		$this->addForeignKey ( 'fk_album_category_1', 'album_category', 'id_album', 'album', 'id', 'RESTRICT', 'CASCADE' );
		$this->addForeignKey ( 'fk_album_category_2', 'album_category', 'id_category', 'category', 'id', 'RESTRICT', 'CASCADE' );
		
		$this->createTable ( 'album_track', array (
				'id_album' => 'int NOT NULL',
				'id_track' => 'int NOT NULL' 
		) );
		
		$this->addForeignKey ( 'fk_album_track_1', 'album_track', 'id_album', 'album', 'id', 'RESTRICT', 'CASCADE' );
		$this->addForeignKey ( 'fk_album_track_2', 'album_track', 'id_track', 'track', 'id', 'RESTRICT', 'CASCADE' );
		
		$this->createTable ( 'tag', array (
				'id' => 'pk',
				'name' => 'string NOT NULL' 
		) );
		
		$this->createTable ( 'track_tag', array (
				'id_track' => 'int NOT NULL',
				'id_tag' => 'int NOT NULL' 
		) );
		
		$this->addForeignKey ( 'fk_track_tag_1', 'track_tag', 'id_track', 'track', 'id', 'RESTRICT', 'CASCADE' );
		$this->addForeignKey ( 'fk_track_tag_2', 'track_tag', 'id_tag', 'tag', 'id', 'RESTRICT', 'CASCADE' );
		
		$this->createTable ( 'file', array (
				'id' => 'pk',
				'name' => 'string NOT NULL',
				'created_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
				'updated_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP' 
		) );
		
		$this->createTable ( 'track_file', array (
				'id_track' => 'int NOT NULL',
				'id_file' => 'int NOT NULL',
				'type' => 'enum("full","short","loop","preview_full","preview_short","preview_loop","open_mix","open_mix_plus")' 
		) );
		
		$this->createTable ( 'contact', array (
				'id' => 'pk',
				'name' => 'string NOT NULL',
				'email' => 'string NOT NULL',
				'phone' => 'string NOT NULL',
				'subject' => 'string NOT NULL',
				'message' => 'text NOT NULL',
				'created_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP' 
		) );
		
		$this->createTable ( 'newsletter', array (
				'id' => 'pk',
				'email' => 'string NOT NULL',
				'created_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP' 
		) );
	}
	public function down() {
		echo "m150426_180256_init cannot be reverted.\n";
		
		return false;
	}
	
	/*
	 * // Use safeUp/safeDown to run migration code within a transaction
	 * public function safeUp()
	 * {
	 * }
	 *
	 * public function safeDown()
	 * {
	 * }
	 */
}
