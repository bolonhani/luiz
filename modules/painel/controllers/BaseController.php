<?php
namespace app\modules\painel\controllers;

use yii\base\Controller;

abstract class BaseController extends Controller{
    public $layout = "main";	
}