<?php
namespace app\controllers;

use yii\web\Controller;

class SectionController extends Controller
{

    public function actionIndex($type)
    {
        return $this->render('index',['type'=>$type]);
    }
}