<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "album".
 *
 * @property integer $id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AlbumCategory[] $albumCategories
 * @property AlbumTrack[] $albumTracks
 */
class Album extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'album';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbumCategories()
    {
        return $this->hasMany(AlbumCategory::className(), ['id_album' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbumTracks()
    {
        return $this->hasMany(AlbumTrack::className(), ['id_album' => 'id']);
    }
}
