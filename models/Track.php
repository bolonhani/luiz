<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "track".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $tech_description
 * @property integer $time
 * @property integer $bpm
 * @property double $standard_price
 * @property double $premium_price
 * @property double $master_price
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AlbumTrack[] $albumTracks
 * @property TrackTag[] $trackTags
 */
class Track extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'track';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'tech_description', 'time', 'bpm', 'standard_price', 'premium_price', 'master_price'], 'required'],
            [['time', 'bpm'], 'integer'],
            [['standard_price', 'premium_price', 'master_price'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'description', 'tech_description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'tech_description' => 'Tech Description',
            'time' => 'Time',
            'bpm' => 'Bpm',
            'standard_price' => 'Standard Price',
            'premium_price' => 'Premium Price',
            'master_price' => 'Master Price',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbumTracks()
    {
        return $this->hasMany(AlbumTrack::className(), ['id_track' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrackTags()
    {
        return $this->hasMany(TrackTag::className(), ['id_track' => 'id']);
    }
}
