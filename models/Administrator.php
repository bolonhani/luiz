<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "administrator".
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $created_at
 * @property string $updated_at
 */
class Administrator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'administrator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password_hash'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['username', 'password_hash'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
